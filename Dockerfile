FROM openjdk:8-jdk-alpine
MAINTAINER ahmedsaka91@gmail.com
COPY Todo-app-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar", '-Dspring.profiles.active=docker',"/app.jar"]