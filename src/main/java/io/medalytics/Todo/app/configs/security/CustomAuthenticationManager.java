package io.medalytics.Todo.app.configs.security;

import io.jsonwebtoken.Claims;
import io.medalytics.Todo.app.exception.exceptions.NotFoundException;
import io.medalytics.Todo.app.model.User;
import io.medalytics.Todo.app.service.JwtUtils;
import io.medalytics.Todo.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;

@Configuration
public class CustomAuthenticationManager implements AuthenticationManager {
        @Autowired
        JwtUtils jwtUtils;

        @Autowired
        UserService userService;

        @Override
        public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            String authToken = authentication.getCredentials().toString();

            if (jwtUtils.isTokenExpired(authToken)) return null;

            Claims claims = jwtUtils.extractAllClaims(authToken);
            String subject = claims.getSubject();

            User user = userService.findUserByEmail(subject);

            if (user != null) {
                return new UsernamePasswordAuthenticationToken(user.getEmail(), null,
                        Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user.getRole()))
                );
            }
            throw new NotFoundException(String.format("User %s not found", subject));
        }
}
