package io.medalytics.Todo.app.controller;

import io.medalytics.Todo.app.enums.SortOrder;
import io.medalytics.Todo.app.model.Todo;
import io.medalytics.Todo.app.model.requests.TodoRequest;
import io.medalytics.Todo.app.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/todo/todo")
public class TodoController {

    private TodoService todoService;

    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Todo create(@RequestBody TodoRequest request) {
       return todoService.create(request);
    }

    @GetMapping(value = "/view", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Todo> viewTodosWithParams(@RequestParam String start_date,
                                          @RequestParam String end_date,
                                          @RequestParam int pageSize,
                                          @RequestParam int pageNumber,
                                          @RequestParam String order) {
        TodoRequest request = new TodoRequest();
        request.setStartDate(LocalDateTime.parse(start_date));
        request.setEndDate(LocalDateTime.parse(end_date));
        Pageable pageable;
        if (SortOrder.ASC.name().equals(order)){
            pageable = PageRequest.of(pageNumber, pageSize, Sort.by("start_date").ascending());
        } else {
            pageable = PageRequest.of(pageNumber, pageSize, Sort.by("start_date").descending());
        }
        return todoService.viewTodos(request, pageable);
    }

    @DeleteMapping("/delete")
    public void deleteById(@RequestParam Long id){
        todoService.deleteById(id);
    }
}
