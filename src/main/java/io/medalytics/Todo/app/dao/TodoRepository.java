package io.medalytics.Todo.app.dao;

import io.medalytics.Todo.app.model.Todo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM todo t WHERE t.start_date >= ?1 AND t.end_date <= ?2",
        countQuery = "SELECT COUNT(*) FROM todo t WHERE t.start_date >= ?1 AND t.end_date <= ?2")
    Page<Todo> viewTodosByDate(LocalDateTime startDate, LocalDateTime endDate, Pageable pageable);
}
