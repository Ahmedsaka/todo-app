package io.medalytics.Todo.app.dao;

import io.medalytics.Todo.app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM users u WHERE u.username = ?1 OR u.email = ?2")
    public Optional<User> findUserByUsernameOrEmail(String username, String email);
}
