package io.medalytics.Todo.app.enums;

import java.util.EnumSet;

public enum Role {
    ADMIN, OPERATOR, SUPER_ADMIN, USER;

    public static EnumSet getAllRole() {
        return EnumSet.allOf(Role.class);
    }
}

