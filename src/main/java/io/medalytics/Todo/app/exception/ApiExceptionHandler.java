package io.medalytics.Todo.app.exception;

import io.medalytics.Todo.app.exception.exceptions.AuthenticationException;
import io.medalytics.Todo.app.exception.exceptions.DuplicateException;
import io.medalytics.Todo.app.exception.exceptions.InvalidDateException;
import io.medalytics.Todo.app.exception.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(DuplicateException.class)
    public ResponseEntity<Error> handleDuplicateException(DuplicateException ex) {
        Error error = new Error();
        error.setDescription(ex.getMessage());
        error.setStatus(HttpStatus.ALREADY_REPORTED);
        error.setLocalDateTime(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Error> handleNotFoundException(NotFoundException ex) {
        Error error = new Error();
        error.setDescription(ex.getMessage());
        error.setStatus(HttpStatus.NOT_FOUND);
        error.setLocalDateTime(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<Error> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        Error error = new Error();
        error.setDescription("Your JSON is badly formatted!!!");
        error.setStatus(HttpStatus.BAD_REQUEST);
        error.setLocalDateTime(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidDateException.class)
    public ResponseEntity<Error> handleInvalidDateException(InvalidDateException ex) {
        Error error = new Error();
        error.setDescription(ex.getMessage());
        error.setStatus(HttpStatus.BAD_REQUEST);
        error.setLocalDateTime(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Error> handleAuthenticationException(AuthenticationException ex) {
        Error error = new Error();
        error.setDescription(ex.getMessage());
        error.setStatus(HttpStatus.UNAUTHORIZED);
        error.setLocalDateTime(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }
}
