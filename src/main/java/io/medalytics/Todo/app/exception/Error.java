package io.medalytics.Todo.app.exception;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Error {
    private String description;
    private HttpStatus status;
    private LocalDateTime localDateTime;
}
