package io.medalytics.Todo.app.model.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class TodoRequest {
    private String requestId;
    @JsonProperty(value = "description")
    private String description;
    @JsonProperty(value = "startDate")
    private LocalDateTime startDate;
    @JsonProperty(value = "endDate")
    private LocalDateTime endDate;
}
