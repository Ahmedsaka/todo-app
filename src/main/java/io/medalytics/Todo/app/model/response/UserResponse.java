package io.medalytics.Todo.app.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private String requestId;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String token;

    public UserResponse(String firstName, String lastName, String email, String authToken) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.token = authToken;
    }
}
