package io.medalytics.Todo.app.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Component
public class HMACUtils {
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final String HMAC_512 = "HmacSHA512";

    @Value("${hmac.security.key}")
    private String key;
    private  byte[] hmacSha512(String data) {
        try{
            SecretKey keySpec = new SecretKeySpec(
                    key.getBytes(DEFAULT_ENCODING),
                    HMAC_512);
            Mac mac = Mac.getInstance(HMAC_512);
            mac.init(keySpec);
            return mac.doFinal(data.getBytes(DEFAULT_ENCODING));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    private String signatureString(byte[] hexData) {
        return  Base64.getEncoder().encodeToString(hexData);
    }

    public String encode(String password) {
        return signatureString(hmacSha512(password));
    }

    public boolean matches(String data, String signature){
        byte[] convertDataToSignature = hmacSha512(data);
        return signatureString(convertDataToSignature).equals(signature);
    }

}
