package io.medalytics.Todo.app.service;

import io.medalytics.Todo.app.dao.TodoRepository;
import io.medalytics.Todo.app.exception.exceptions.InvalidDateException;
import io.medalytics.Todo.app.model.Todo;
import io.medalytics.Todo.app.model.requests.TodoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TodoService {

    private TodoRepository todoRepository;
    private UserService userService;

    @Autowired
    public TodoService(TodoRepository todoRepository, UserService userService) {
        this.todoRepository = todoRepository;
        this.userService = userService;
    }

    public Todo create(TodoRequest request) {
        Todo todo = new Todo();
        todo.setDescription(request.getDescription());
        todo.setStartDate(LocalDateTime.now());
        todo.setEndDate(LocalDateTime.now().plusDays(1));
        todo.setUser(userService.findById(1L));

        return todoRepository.save(todo);
    }

    public Page<Todo> viewTodos(TodoRequest request, Pageable pageable) {
        if (request.getStartDate() != null && request.getEndDate() != null){
            if (request.getStartDate().isAfter(request.getEndDate())) {
                throw new InvalidDateException("Start date cannot be greater than end date");
            }
            return todoRepository.viewTodosByDate(request.getStartDate(), request.getEndDate(), pageable);
        }
        return null;
    }

    public void deleteById(Long id) {
        todoRepository.deleteById(id);
    }
}
