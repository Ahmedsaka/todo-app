package io.medalytics.Todo.app.service;

import io.medalytics.Todo.app.enums.Role;
import io.medalytics.Todo.app.dao.UserRepository;
import io.medalytics.Todo.app.exception.exceptions.AuthenticationException;
import io.medalytics.Todo.app.exception.exceptions.DuplicateException;
import io.medalytics.Todo.app.exception.exceptions.NotFoundException;
import io.medalytics.Todo.app.model.User;
import io.medalytics.Todo.app.model.requests.UserRequest;
import io.medalytics.Todo.app.model.response.UserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    UserRepository userRepository;
    HMACUtils hmacUtils;
    JwtUtils jwtUtils;

    @Autowired
    public UserService(UserRepository userRepository, HMACUtils hmacUtils, JwtUtils jwtUtils) {
        this.userRepository = userRepository;
        this.hmacUtils = hmacUtils;
        this.jwtUtils = jwtUtils;
    }

    public UserResponse createUser(UserRequest request) {
        Optional<User> optionalExistingUser =
                userRepository.findUserByUsernameOrEmail(request.getUsername(), request.getEmail());
        if (optionalExistingUser.isPresent()) {
            throw new DuplicateException(String.format("User %s already exists", request.getUsername()));
        }
        User user = new User();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(hmacUtils.encode(request.getPassword()));
        user.setRole(request.getRole() != null ? request.getRole() : Role.USER.name());
        user.setImageUrl("");

        User savedUser = userRepository.save(user);

        LOG.info(String.format("Successfully created user %s", savedUser.getUsername()));

        String authToken = jwtUtils.generateToken(savedUser.getEmail(), Role.valueOf(savedUser.getRole()));

        UserResponse response = new UserResponse();
        response.setFirstName(savedUser.getFirstName());
        response.setLastName(savedUser.getLastName());
        response.setUsername(savedUser.getUsername());
        response.setEmail(savedUser.getEmail());
        response.setToken(authToken);
        return response;
    }

    public UserResponse findUser(UserRequest request){
        if (!Role.getAllRole().contains(request.getRole())) {
            throw new NotFoundException(String.format("No such Role %s", request.getRole()));
        }
        Optional<User> optionalUser = Optional.of(userRepository.findUserByUsernameOrEmail(request.getUsername(), request.getEmail())
                .orElseThrow(() -> new NotFoundException(String.format("User %s does not exists", request.getUsername())
                )));
        User savedUser = optionalUser.get();

        UserResponse response = new UserResponse();
        response.setFirstName(savedUser.getFirstName());
        response.setLastName(savedUser.getLastName());
        response.setUsername(savedUser.getUsername());
        response.setEmail(savedUser.getEmail());
        response.setRequestId(request.getRequestId());
        return response;
    }


    public UserResponse login(UserRequest request) {
        Optional<User> optionalUser = userRepository.findUserByUsernameOrEmail(request.getUsername(), request.getEmail());
        User user = null;
        if (optionalUser.isPresent()){
            user = optionalUser.get();
        }
        assert user != null;
        if (hmacUtils.matches(request.getPassword(), user.getPassword())){
            String authToken = jwtUtils.generateToken(request.getEmail(), Role.valueOf(request.getRole()));
            LOG.info("Successful login by user {}", user.getEmail());
            return new UserResponse(user.getFirstName(), user.getLastName(), user.getEmail(), authToken);
        } throw new AuthenticationException(String.format("User %s is not authenticated", request.getEmail()));
    }

    public User findById(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        return optionalUser.orElse(null);
    }

    public User findUserByEmail(String email) {
        Optional<User> optionalUser = userRepository.findUserByUsernameOrEmail(email, email);
        return optionalUser.orElse(null);
    }
}
