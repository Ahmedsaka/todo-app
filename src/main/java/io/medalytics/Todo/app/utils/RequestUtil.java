package io.medalytics.Todo.app.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class RequestUtil {
    public static String secureRandomAlphabetic(int count){
        try {
            return RandomStringUtils.random(count, 0, 0, true, false, (char[])null, SecureRandom.getInstanceStrong());
        }
        catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String secureRandomNumeric(int count) {
        try {
            return RandomStringUtils.random(count, 0, 0, false, true, (char[])null, SecureRandom.getInstanceStrong());
        }
        catch (NoSuchAlgorithmException e){
            return "";
        }
    }
}
