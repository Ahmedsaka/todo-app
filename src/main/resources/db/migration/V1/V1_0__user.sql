CREATE TABLE IF NOT EXISTS users (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL UNIQUE,
    role VARCHAR(20) NOT NULL,
    password VARCHAR(100),
    image_url VARCHAR(200)
);

CREATE INDEX IF NOT EXISTS email ON users(email);
CREATE INDEX IF NOT EXISTS username ON users(username);

INSERT INTO users ( first_name, last_name, username, email, role, password, image_url)
VALUES ('admin', 'admin', 'adminUser', 'admin@user.com', 'ADMIN', 'password', '');

INSERT INTO users ( first_name, last_name, username, email, role, password, image_url)
VALUES ('Test1', 'Test', 'tester1', 'test1@test.com', 'USER', 'password', '');

INSERT INTO users ( first_name, last_name, username, email, role, password, image_url)
VALUES ('Test2', 'Test', 'tester2', 'test2@test.com', 'USER', 'password', '');