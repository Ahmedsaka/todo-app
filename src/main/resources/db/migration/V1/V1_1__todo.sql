CREATE TABLE IF NOT EXISTS todo (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(500) NOT NULL,
    start_date DATE,
    end_date DATE,
    user_id BIGINT,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO todo ( description, start_date, end_date, user_id)
VALUES ('Testing 1', CURRENT_DATE(), CURRENT_DATE(), 1);

INSERT INTO todo ( description, start_date, end_date, user_id)
VALUES ('Testing 2', CURRENT_DATE(), CURRENT_DATE(), 2);

INSERT INTO todo ( description, start_date, end_date, user_id)
VALUES ('Testing 3', CURRENT_DATE(), CURRENT_DATE(), 3);


CREATE INDEX IF NOT EXISTS user_id ON todo(user_id);